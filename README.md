# Liste des Lives et sujets

## Lives passés

### ZABBIX

- Agent ZABBIX - Actif vs Passif: le Mardi 7 Avril 2020
- CACTI vs ZABBIX: le Jeudi 9 Avril 2020
- Les LLD avec ZABBIX: le Jeudi 23 Avril 2020
- Utiliser l'item HTTP Agent: le Mardi 05 Mai 2020


### Nouvelle version de ZABBIX

- ZABBIX 5.0: le Mardi 21 Avril 2020
- ZABBIX 5.0: le Jeudi 30 Avril 2020 (avec la participation du créateur de ZABBIX Alexeï Vladishev)



### Sujet à aborder

- ZABBIX, GIT et Ansible AWX
- Le SNMP avec ZABBIX
- Prometheus vs ZABBIX
- Traiter un JSON avec ZABBIX
- Est-ce que ZABBIX peut tourner dans K8S ?
- Est-ce que ZABBIX peut récupérer les infos des ressources K8S ?
- Items dépendants et monitoring de MySQL
- ZABBIX et Grafana
- API ZABBIX
- Monitoring de Docker
- Maintenance
- Regular expressions
- Event acknowledgement workflow
- XML export/import
- Notifications et escalades
- Monitoring web
- Le pre-processing
- Les tags
- Le chiffrement
- Les decouvertes réseau
- Optimisation de Zabbix
- Zabbix forecast
- 
